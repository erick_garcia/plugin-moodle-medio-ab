//Esta función muestra los comentarios con formato HTML
function format_comentarios(comentario, usuario, fecha) {
    $('#comments').append(
        '<div>' +
            '<p>' + comentario + '</p>' +
            '<p><em>' +  usuario + '</em></p>' +
            '<span>' +  fecha + '</span>' +
        '</div>'
    );
}

$( document ).ready(function() {
    var act1 =  maybe.urlbase + $('#u1a15').attr('action') + maybe.u1a15;
    $('#u1a15').attr('action', act1);

    var act2 =  maybe.urlbase + $('#u1a16').attr('action') + maybe.u1a16;
    $('#u1a16').attr('action', act2);

    var act3 =  maybe.urlbase + $('#u2a15').attr('action') + maybe.u2a15;
    $('#u2a15').attr('action', act3);

    var act4 =  maybe.urlbase + $('#u2a16').attr('action') + maybe.u2a16;
    $('#u2a16').attr('action', act4);

    var act5 =  maybe.urlbase + $('#u2a17').attr('action') + maybe.u2a17;
    $('#u2a17').attr('action', act5);

    var act6 =  maybe.urlbase + $('#u4a14').attr('action') + maybe.u4a14;
    $('#u4a14').attr('action', act6);

    var act7 =  maybe.urlbase + $('#u4a15').attr('action') + maybe.u4a15;
    $('#u4a15').attr('action', act7);

    var actionActual = $('.formularioaenviar').attr('action');
    var idActual = actionActual.split('id=');

    //Hacemos una petición GET para recuperar un JSON 
    $.get( maybe.urlbase + "mod/actividadescurso/comments.php", { idActividad: idActual[1]}, function( data ) {
        var comment = JSON.parse(data);
        for(key in comment) {
            //mostramos los comentarios
            format_comentarios(comment[key].comentario, comment[key].nombre_usuario, comment[key].fecha);
        }
    });

    //Cuando se haga click en el botón del formulario enviamos el comentario para que sea guardado
    $('.formularioaenviar').submit(function() {
        $.ajax({
            type: 'POST',
            url: $(this).attr('action'),
            data: $(this).serialize(),
            success: function(data) {
                if(data == false) {
                    alert('Es necesario que escribas un comentario.');
                } else {
                    var mostrarComentario = data.split('|');
                    format_comentarios(mostrarComentario[0], mostrarComentario[1], mostrarComentario[2]);
                }
            }
        })
        return false;
    });
});