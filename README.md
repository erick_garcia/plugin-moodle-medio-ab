# Plugin de moodle

La carpeta **actividadescurso** se debe comprimir en formato **.zip** para que posteriormente se instale en moodle.

El archivo **comentarios.js** se debe integrar con los demas archivos del curso.

El archivo **html_formulario.html** contiene el formulario que se utiliza para el envio de los comentarios solo es necesario copiar el código y agregarlo dentro de los archivos html del curso.