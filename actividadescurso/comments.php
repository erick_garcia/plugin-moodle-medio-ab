<?php
require_once('../../config.php');
if ( isloggedin() ) {
	//Realizamos una consulta a la tabla que contiene los comentarios y los comparamos con el id de la actividad
	$comentarios = $DB->get_records_sql('SELECT id, comentario, nombre_usuario, fecha FROM mdl_actividadescurso_comentarios WHERE idactividad = "'.$_GET['idActividad'].'"');
	//Convertimos el resultado a un JSON
	$json_comentarios = json_encode($comentarios, JSON_UNESCAPED_UNICODE);
	//Imprimimos el JSON
	print_r($json_comentarios);
} else {
	header('Location: index.php');
}