<?php
/**
 * Provides code to be executed during the module uninstallation
 *
 * @see uninstall_plugin()
 *
 * @package    mod_actividadescurso
 * @copyright  2018 Erik Garcia <erick_garcia@cuaed.unam.mx>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

/**
 * Custom uninstallation procedure
 */
function xmldb_actividadescurso_uninstall() {
    return true;
}
