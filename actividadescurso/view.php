<?php
/**
 * Prints a particular instance of actividadescurso
 *
 * You can have a rather longer description of the file as well,
 * if you like, and it can span multiple lines.
 *
 * @package    mod_actividadescurso
 * @copyright  2018 Erik Garcia <erick_garcia@cuaed.unam.mx>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once(dirname(dirname(dirname(__FILE__))).'/config.php');
require_once(dirname(__FILE__).'/lib.php');

$id = optional_param('id', 0, PARAM_INT); // Course_actividadescurso ID, or
$n  = optional_param('n', 0, PARAM_INT);  // ... actividadescurso instance ID - it should be named as the first character of the module.

if ($id) {
    $cm         = get_coursemodule_from_id('', $id, 0, false, MUST_EXIST);
    $course     = $DB->get_record('course', array('id' => $cm->course), '*', MUST_EXIST);
    $actividadescurso  = $DB->get_record('actividadescurso', array('id' => $cm->instance), '*', MUST_EXIST);
} else if ($n) {
    $actividadescurso  = $DB->get_record('actividadescurso', array('id' => $n), '*', MUST_EXIST);
    $course     = $DB->get_record('course', array('id' => $actividadescurso->course), '*', MUST_EXIST);
    $cm         = get_coursemodule_from_instance('actividadescurso', $actividadescurso->id, $course->id, false, MUST_EXIST);
} else {
    error('You must specify a course_module ID or an instance ID');
}

require_login($course, true, $cm);

$event = \mod_actividadescurso\event\course_module_viewed::create(array(
    'objectid' => $PAGE->cm->instance,
    'context' => $PAGE->context,
));
$event->add_record_snapshot('course', $PAGE->course);
$event->add_record_snapshot($PAGE->cm->modname, $actividadescurso);
$event->trigger();

// Print the page header.

$PAGE->set_url('/mod/actividadescurso/view.php', array('id' => $cm->id));
$PAGE->set_title(format_string($actividadescurso->name));
$PAGE->set_heading(format_string($course->fullname));

/*
 * Other things you may want to set - remove if not needed.
 * $PAGE->set_cacheable(false);
 * $PAGE->set_focuscontrol('some-html-id');
 * $PAGE->add_body_class('actividadescurso-'.$somevar);
 */

// Output starts here.
echo $OUTPUT->header();

// Conditions to show the intro can change to look for own settings or whatever.
if ($actividadescurso->intro) {
    echo $OUTPUT->box(format_module_intro('actividadescurso', $actividadescurso, $cm->id), 'generalbox mod_introbox', 'actividadescursointro');
}

//echo $OUTPUT->heading('Aqui se muestran los comentarios');

//Realizamos una consulta a la tabla que contiene los comentarios y los comparamos con el id de la actividad
$comentarios = $DB->get_records_sql('SELECT id, comentario, nombre_usuario, fecha FROM mdl_actividadescurso_comentarios WHERE idactividad = "'.$_GET['id'].'"');
//Convertimos el resultado a un JSON
$json_comentarios = json_encode($comentarios, JSON_UNESCAPED_UNICODE);
$comentarios_por_actividad = json_decode($json_comentarios, true);


//Mostramos los comentarios obtenidos en un tabla
$tabla = '
    <table border="3">
        <thead>
            <tr>
                <th style="padding:1.5em; text-align:center;">Comentario</th>
                <th style="padding:1.5em; text-align:center;">Usuario</th>
                <th style="text-align:center;">Fecha</th>
            </tr>
        </thead>
        <tbody>
';

foreach ($comentarios_por_actividad as $key => $value) {
    $tabla .= '<tr>
                    <td style="padding:1.5em;">' . $value['comentario'] . '</td>
                    <td style="padding:1.5em;">' . $value['nombre_usuario'] . '</td>
                    <td style="padding:1.5em;">' . $value['fecha'] . '</td>
                </tr>';
}
$tabla .= '</tbody></table>';
//Imprimimos la tabla
printf($tabla);

// Finish the page.
echo $OUTPUT->footer();
