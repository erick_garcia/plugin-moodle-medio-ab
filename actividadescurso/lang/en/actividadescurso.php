<?php
/**
 * English strings for actividadescurso
 *
 * You can have a rather longer description of the file as well,
 * if you like, and it can span multiple lines.
 *
 * @package    mod_nactividadescurso
 * @copyright  2018 Erik Garcia <erick_garcia@cuaed.unam.mx>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['modulename'] = 'Comentarios';
$string['modulenameplural'] = 'actividadescursos';
$string['modulename_help'] = 'Este modulo permite visualizar los comentarios que los alumnos realizan en las actividades.';
$string['actividadescurso:addinstance'] = 'Add a new actividadescurso';
$string['actividadescurso:submit'] = 'Submit actividadescurso';
$string['actividadescurso:view'] = 'View actividadescurso';
$string['actividadescursofieldset'] = 'Custom example fieldset';
$string['actividadescursoname'] = 'Nombre de la actividad';
$string['actividadescursoname_help'] = 'This is the content of the help tooltip associated with the newmodulename field.';
$string['actividadescurso'] = 'actividadescurso';
$string['pluginadministration'] = 'actividadescurso administration';
$string['pluginname'] = 'Comentarios';
