<?php
require_once('../../config.php');

if (isloggedin()) {
	if ( !empty($_POST)) {
		//Guardamos el comentario que fue enviado por POST
		$comentario = $_POST['txtComentario'];
		$nombre = $USER->firstname . " " . $USER->lastname;
		$id_actividad = $_GET['id'];
		$id_usuario = $USER->id;

		//Si el contenido de la petición esta vacío retornamos false.
		if (empty($_POST['txtComentario'])) {
			return false;
		} else {
			//Insertamos los datos en la tabla actividadescurso_comentarios
			$record = new stdClass();
			$record->comentario = $comentario;
			$record->nombre_usuario = $nombre;
			$record->idactividad = $id_actividad;
			$record->idusuario = $id_usuario;
			$resul = $DB->insert_record('actividadescurso_comentarios', $record, false);
			print($comentario.'|'.$nombre.'|'. date('Y-m-d H:i:s'));
		}
	} 
}else {
	header('Location: index.php');
}

