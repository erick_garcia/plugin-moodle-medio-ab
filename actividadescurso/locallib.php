<?php
/**
 * Internal library of functions for module actividadescurso
 *
 * All the actividadescurso specific functions, needed to implement the module
 * logic, should go here. Never include this file from your lib.php!
 *
 * @package    mod_actividadescurso
 * @copyright  2018 Erik Garcia <erick_garcia@cuaed.unam.mx>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

/*
 * Does something really useful with the passed things
 *
 * @param array $things
 * @return object
 *function actividadescurso_do_something_useful(array $things) {
 *    return new stdClass();
 *}
 */
